# Use the official Jenkins LTS image as a base
FROM jenkins/jenkins:lts

USER root

# Install dependencies and OpenJDK 11
RUN apt-get update && \
    apt-get install -y wget && \
    mkdir -p /usr/local/openjdk-11 && \
    cd /usr/local/openjdk-11 && \
    wget https://download.java.net/java/GA/jdk11/9/GPL/openjdk-11.0.2_linux-x64_bin.tar.gz && \
    tar -zxvf openjdk-11.0.2_linux-x64_bin.tar.gz --strip-components=1 && \
    rm openjdk-11.0.2_linux-x64_bin.tar.gz

# Switch back to the Jenkins user
USER jenkins