package com.muf.springjpaimplement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RequestNewDiscount {
    @NotNull
    private String discountCode;

    private String discountDesc;

    private Integer discountQty;

    private Integer discountExpDate;

    private Integer discountRate;

    private Integer discountMinPrice;

    private Integer discountMaxRate;

    private String merchantUuid;

}
