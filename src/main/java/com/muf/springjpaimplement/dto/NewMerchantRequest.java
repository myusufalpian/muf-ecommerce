package com.muf.springjpaimplement.dto;

import lombok.*;

@Setter
@Getter
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewMerchantRequest {
    private String merchantCategoryUuid;
    private String merchantDetaillName;
    private String merchantDetailPhone;
    private String merchantDetailCity;
    private String merchantDetailProvince;
    private String merchantDetailPostCode;
    private String merchantDetailAddress;
}
