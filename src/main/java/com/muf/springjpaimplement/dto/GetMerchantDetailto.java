package com.muf.springjpaimplement.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public interface GetMerchantDetailto {
    String getMerchantUuid();
    String getMerchantDetailName();
    String getMerchantDetailCity();
    String getMerchantCategoryUuid();
    String getMerchantCategoryName();
    String getMerchantDetailPhone();
    String getMerchantDetailProvince();
    String getMerchantDetailPostCode();
    String getMerchantDetailAddress();
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    Date getMerchantCreatedDate();
}
