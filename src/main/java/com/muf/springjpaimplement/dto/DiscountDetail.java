package com.muf.springjpaimplement.dto;

import java.util.Date;

public class DiscountDetail {
    private String discountUuid;

    private String discountCode;

    private String discountDesc;

    private Integer discountQty;

    private Date discountExpDate;

    private Integer discountRate;

    private Integer discountMinPrice;

    private Integer discountMaxRate;

}
