package com.muf.springjpaimplement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RequestNewItem {
    private String categoryUuid;
    private String merchantUuid;
    private String itemName;
    private Integer itemStock;
    private Integer itemPrice;
}
