package com.muf.springjpaimplement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestRegister {
    private String userFirstName;
    private String userLastName;
    private String userEmail;
    private String userUsername;
    private String userPhone;
    private String userPass;
    private String userConfirmPass;
}
