package com.muf.springjpaimplement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResponseFinalItemCart {
    private String itemUuid;
    private String itemName;
    private Integer itemQty;
    private Integer itemPrice;
    private Integer itemTotalPrice;

}
