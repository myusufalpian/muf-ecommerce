package com.muf.springjpaimplement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestUpdate {
    private String userUuid;
    private String userFirstName;
    private String userLastName;
    private String userPhone;
}
