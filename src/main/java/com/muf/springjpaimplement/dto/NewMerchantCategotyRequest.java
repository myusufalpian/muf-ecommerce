package com.muf.springjpaimplement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class NewMerchantCategotyRequest {
    private String merchantCategoryName;
}
