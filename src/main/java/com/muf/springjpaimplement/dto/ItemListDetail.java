package com.muf.springjpaimplement.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public interface ItemListDetail {
    String itemUuid();
    String itemName();
    String categoryUuid();
    String merchantUuid();
    Integer itemStock();
    Integer itemPrice();
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    Date createdDate();
}
