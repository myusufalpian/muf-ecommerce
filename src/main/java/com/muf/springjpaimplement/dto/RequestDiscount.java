package com.muf.springjpaimplement.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class RequestDiscount {
    @NotNull
    private String discountCode;

    private String discountDesc;

    private Integer discountQty;

    private Integer discountExpDate;

    private Integer discountRate;

    private Integer discountMinPrice;

    private Integer discountMaxRate;

}
