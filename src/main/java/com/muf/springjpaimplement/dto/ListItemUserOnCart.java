package com.muf.springjpaimplement.dto;

public interface ListItemUserOnCart {
    String getItemCartUuid();
    String getItemName();
    Integer getItemPrice();
    Integer getItemQty();
    String getItemUuid();
    String getCartUuid();

}
