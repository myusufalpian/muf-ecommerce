package com.muf.springjpaimplement.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ItemDetail {
    private String itemUuid;
    private String itemName;
    private String categoryUuid;
    private String merchantUuid;
    private Integer itemStock;
    private Integer itemPrice;
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date createdDate;
}
