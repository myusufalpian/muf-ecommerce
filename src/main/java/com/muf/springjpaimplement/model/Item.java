package com.muf.springjpaimplement.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "itms", schema = "public")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial", name = "itms_id")
    private Integer itemId;

    @Column(name = "itms_uuid")
    private String itemUuid;

    @Column(name = "itms_name")
    private String itemName;

    @Column(name = "itms_stck")
    private Integer itemStock;

    @Column(name = "mrchnt_id")
    private Integer merchantId;

    @Column(name = "ctegry_id")
    private Integer categoryId;

    @Column(name = "itms_price")
    private Integer itemPrice;

    @Column(name = "itms_status")
    private Integer itemStatus;

    @Column(name = "created_by")
    private Integer createdBy;

    @Column(name = "created_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date createdDate;

    @Column(name = "updated_by")
    private Integer updatedBy;

    @Column(name = "updated_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date updatedDate;
}
