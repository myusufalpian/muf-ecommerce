package com.muf.springjpaimplement.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "usrs", schema = "public")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial", name = "usrs_id")
    private Integer userId;

    @Column(name = "usrs_uuid")
    private String userUuid;

    @Column(name = "usrs_first_name")
    private String userFirstName;

    @Column(name = "usrs_last_name")
    private String userLastName;
    
    @Column(name = "usrs_email")
    private String userEmail;

    @Column(name = "usrs_usrnme")
    private String userUsername;

    @Column(name = "usrs_phone")
    private String userPhone;

    @Column(name = "usrs_status")
    private Integer userStatus;

    @Column(name = "usrs_pass")
    private String userPass;

    @Column(name = "created_by")
    private Integer createdBy;

    @Column(name = "created_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date createdDate;

    @Column(name = "updated_by")
    private Integer updatedBy;

    @Column(name = "updated_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date updatedDate;

}