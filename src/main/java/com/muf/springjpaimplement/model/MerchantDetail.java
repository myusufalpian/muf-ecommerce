package com.muf.springjpaimplement.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "mrchnt_detl", schema = "public")
public class MerchantDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial", name = "mrchnt_detl_id")
    private Integer merchantDetailId;

    @Column(name = "mrchnt_detl_uuid")
    private String merchantDetailUuid;

    @Column(name = "mrchnt_id")
    private Integer merchantId;

    @Column(name = "mrchnt_detl_nme")
    private String merchantDetailName;

    @Column(name = "mrchnt_detl_phons")
    private String merchantDetailPhone;

    @Column(name = "mrchnt_detl_city")
    private String merchantDetailCity;

    @Column(name = "mrchnt_detl_prvnc")
    private String merchantDetailProvince;

    @Column(name = "mrchnt_detl_pst_cde")
    private String merchantDetailPostCode;

    @Column(name = "mrchnt_detl_addrss")
    private String merchantDetailAddress;

    @Column(name = "created_by")
    private Integer createdBy;

    @Column(name = "created_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date createdDate;

    @Column(name = "updated_by")
    private Integer updatedBy;

    @Column(name = "updated_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date updatedDate;

}
