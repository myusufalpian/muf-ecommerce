package com.muf.springjpaimplement.model;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "lkup_mail", schema = "public")
public class LookupMail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial", name = "lkup_mail_id")
    private Integer lookupMailId;

    @Column(name = "lkup_mail_uuid")
    private String lookupMailUuid;

    @Column(name = "lkup_mail_topic")
    private String lookupMailTopic;

    @Column(name = "lkup_mail_subject")
    private String lookupMailSubject;

    @Column(name = "lkup_mail_bbdy")
    private String lookupMailBody;

    @Column(name = "created_by")
    private Integer createdBy;

    @Column(name = "created_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date createdDate;

    @Column(name = "updated_by")
    private Integer updatedBy;

    @Column(name = "updated_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="Asia/Jakarta")
    private Date updatedDate;
}
