package com.muf.springjpaimplement.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.config.JwtUtil;
import com.muf.springjpaimplement.dto.UserDetailDto;
import com.muf.springjpaimplement.model.User;
import com.muf.springjpaimplement.repository.UserRepository;
import com.muf.springjpaimplement.util.GenerateResponse;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final JwtUtil jwtUtil;
    private final ModelMapper mapper;
    public ResponseEntity<String> getDetailUser(String token) throws JsonProcessingException {
        String username = jwtUtil.validateJwt(token);
        if (username.equals("1")) return GenerateResponse.unauthorized("Token invalid", null);
        else if (username.equals("2")) return GenerateResponse.unauthenticated("Token was expired", null);
        Optional<User> user = userRepository.getUserByUsernameOrEmailAndStatus(jwtUtil.validateJwt(token), 0);
        return user.isEmpty() ? GenerateResponse.notFound("User not found", null) : GenerateResponse.success("Get data success", mapper.map(user.get(), UserDetailDto.class));
    }
}
