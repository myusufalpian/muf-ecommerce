package com.muf.springjpaimplement.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.config.JwtUtil;
import com.muf.springjpaimplement.dto.DiscountDetail;
import com.muf.springjpaimplement.dto.RequestDiscount;
import com.muf.springjpaimplement.dto.RequestNewDiscount;
import com.muf.springjpaimplement.model.Discount;
import com.muf.springjpaimplement.repository.DiscountRepository;
import com.muf.springjpaimplement.repository.MerchantRepository;
import com.muf.springjpaimplement.util.GenerateResponse;
import com.muf.springjpaimplement.util.Helper;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DiscountService {

    private final DiscountRepository discountRepository;

    private final MerchantRepository merchantRepository;

    private final ModelMapper mapper;

    private final JwtUtil jwtUtil;

    private final Helper helper;

    public ResponseEntity<String> addDiscount(String token, RequestNewDiscount param) throws JsonProcessingException {
        try {
            String username = jwtUtil.validateJwt(token);
            if (username.equals("1")) return GenerateResponse.unauthorized("Token was invalid", null);
            else if (username.equals("2")) return GenerateResponse.unauthenticated("Token was expired", null);
            Integer userId = helper.getUserId(username);
            if (userId == 0) return GenerateResponse.success("user not found", null);
            Optional<Discount> checkDiscountCode = discountRepository.findDiscountByDiscountCodeAndDiscountStatus(param.getDiscountCode(), 0);
            if (checkDiscountCode.isPresent()) return GenerateResponse.badRequest("Discount code was already taken", null);
            Discount discount = mapper.map(param, Discount.class);
            discount.setDiscountUuid(UUID.randomUUID().toString());
            Date now = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(now);
            calendar.add(Calendar.DAY_OF_WEEK, param.getDiscountExpDate());
            discount.setDiscountExpDate(calendar.getTime());
            discount.setCreatedBy(userId);
            discount.setCreatedDate(now);
            discount.setDiscountStatus(0);
            discountRepository.save(discount);
            return GenerateResponse.created("Success add new discount", null);
        } catch (Exception e) {
            e.printStackTrace();
            return GenerateResponse.error("Internal server error", null);
        }
    }

    public ResponseEntity<String> updateDiscount(String token, String discountUuid, RequestDiscount param) throws JsonProcessingException {
        String username = jwtUtil.validateJwt(token);
        if (username.equals("1")) return GenerateResponse.unauthorized("Token was invalid", null);
        else if (username.equals("2")) return GenerateResponse.unauthenticated("Token was expired", null);
        Integer userId = helper.getUserId(username);
        if (userId == 0) return GenerateResponse.success("user not found", null);
        Optional<Discount> checkDiscount = discountRepository.findDiscountByDiscountCodeAndDiscountStatus(discountUuid, 0);
        if (checkDiscount.isEmpty()) {
            return GenerateResponse.notFound("Discount not found", null);
        }
        checkDiscount.get().setDiscountCode(param.getDiscountCode());
        checkDiscount.get().setDiscountQty(param.getDiscountQty());
        checkDiscount.get().setDiscountRate(param.getDiscountRate());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(checkDiscount.get().getCreatedDate());
        calendar.add(Calendar.DAY_OF_WEEK, param.getDiscountExpDate());
        checkDiscount.get().setDiscountExpDate(calendar.getTime());
        checkDiscount.get().setDiscountMinPrice(param.getDiscountMinPrice());
        checkDiscount.get().setDiscountMaxRate(param.getDiscountMaxRate());
        checkDiscount.get().setUpdatedBy(userId);
        checkDiscount.get().setUpdatedDate(new Date());
        discountRepository.save(checkDiscount.get());
        return GenerateResponse.success("Success update discount", null);
    }

    public ResponseEntity<String> listDiscount(String token) throws JsonProcessingException {
        String username = jwtUtil.validateJwt(token);
        if (username.equals("1")) return GenerateResponse.unauthorized("Token was invalid", null);
        else if (username.equals("2")) return GenerateResponse.unauthenticated("Token was expired", null);
        Integer userId = helper.getUserId(username);
        if (userId == 0) return GenerateResponse.success("user not found", null);
        List<Discount> discount = discountRepository.findDiscountByCreatedByAndDiscountStatus(userId, 0);
        if (discount.isEmpty()) return GenerateResponse.notFound("Discount not found", null);
        List<DiscountDetail> detail = discount.stream().map(x -> mapper.map(x, DiscountDetail.class)).collect(Collectors.toList());
        return GenerateResponse.success("Get data success", detail);
    }

    public ResponseEntity<String> detailDiscount(String discountUuid) throws JsonProcessingException {
        Optional<Discount> checkDiscount = discountRepository.findDiscountByDiscountUuidAndDiscountStatus(discountUuid, 0);
        if (checkDiscount.isEmpty()) return GenerateResponse.notFound("Discount not found", null);
        DiscountDetail detail = mapper.map(checkDiscount.get(), DiscountDetail.class);
        return GenerateResponse.success("Get data success", detail);
    }

    public ResponseEntity<String> deleteDiscount(String discountUuid) throws JsonProcessingException {
        Optional<Discount> discount = discountRepository.findDiscountByDiscountUuidAndDiscountStatus(discountUuid, 0);
        if (discount.isEmpty()) return GenerateResponse.notFound("Discount not found", null);
        discount.get().setDiscountStatus(1);
        return GenerateResponse.success("Delete discount success", null);
    }

}
