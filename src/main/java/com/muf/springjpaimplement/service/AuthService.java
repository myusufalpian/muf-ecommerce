package com.muf.springjpaimplement.service;

import com.muf.springjpaimplement.config.JwtUtil;
import com.muf.springjpaimplement.dto.UserRequestLogin;
import com.muf.springjpaimplement.dto.UserRequestResetPassword;
import com.muf.springjpaimplement.model.LookupMail;
import com.muf.springjpaimplement.model.User;
import com.muf.springjpaimplement.model.UserOtp;
import com.muf.springjpaimplement.repository.LookupMailRepository;
import com.muf.springjpaimplement.repository.UserOtpRepository;
import com.muf.springjpaimplement.util.Helper;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.dto.UserRequestRegister;
import com.muf.springjpaimplement.repository.UserRepository;
import com.muf.springjpaimplement.util.GenerateResponse;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class AuthService {
    private final UserRepository userRepository;
    private final UserOtpRepository userOtpRepository;
    private final JwtUtil jwtUtil;
    private final ModelMapper mapper;
    private final LookupMailRepository lookupMailRepository;
    private final PasswordEncoder encoder;

    @Transactional
    public ResponseEntity<String> register(UserRequestRegister param) throws JsonProcessingException{
        if (param.getUserEmail() == null || param.getUserEmail().equals("")) return GenerateResponse.badRequest("email user cannot be null", null);
        else if (param.getUserUsername() == null || param.getUserUsername().equals("")) return GenerateResponse.badRequest("username user cannot be null", null);
        else if (param.getUserPass() == null || param.getUserPass().equals("") ) return GenerateResponse.badRequest("password user cannot be null", null);
        else if (param.getUserPass().length() < 6) return GenerateResponse.badRequest("Password must have at least > 6 characters", null);
        else if (param.getUserPass().equals(param.getUserConfirmPass()) ) return GenerateResponse.badRequest("password and confirmation password not matched", null);

        Optional<User> checkUserByUsername = userRepository.getUserByUsernameOrEmail(param.getUserUsername());
        if (checkUserByUsername.isPresent() && checkUserByUsername.get().getUserStatus() == 2) return GenerateResponse.badRequest("Username already taken, please activation your account", null);
        else if (checkUserByUsername.isPresent() && checkUserByUsername.get().getUserStatus() == 0) return GenerateResponse.badRequest("Username already taken", null);
        Optional<User> checkUserByEmail = userRepository.getUserByUsernameOrEmailAndStatus(param.getUserEmail(), 0);
        if (checkUserByEmail.isPresent() && checkUserByEmail.get().getUserStatus() == 2) return GenerateResponse.badRequest("Email already taken, please activation your account", null);
        else if (checkUserByEmail.isPresent() && checkUserByEmail.get().getUserStatus() == 0) return GenerateResponse.badRequest("Email already taken", null);

        if (!checkUserByEmail.isPresent() && !checkUserByUsername.isPresent()) return GenerateResponse.notFound("User not found", null);
        User user = mapper.map(param, User.class);
        user.setUserPass(encoder.encode(param.getUserPass()));
        user.setCreatedBy(0);
        user.setUserStatus(2);
        userRepository.save(user);

        UserOtp userOtp = new UserOtp();
        userOtp.setCreatedBy(user.getUserId());
        userOtp.setUserId(user.getUserId());
        userOtp.setCreatedDate(new Date());
        userOtp.setUserOtpExp(Helper.addTimes(userOtp.getCreatedDate(), "minutes", 3));
        userOtp.setUserOtpCode(Helper.generateOTP(6));
        userOtp.setUserOtpStatus(0);
        userOtpRepository.save(userOtp);

        Optional<LookupMail> mail = lookupMailRepository.findLookupMailByLookupMailTopic("url-reset-password");
//        sendMail(param.getUserEmail(), mail.get().getLookupMailSubject(), mail.get().getLookupMailBody().replace("$otp", Helper.generateOTP(6)));
        return GenerateResponse.success("Register user success, please check your email for activated user", null);
    }

    public ResponseEntity<String> login(UserRequestLogin param) throws JsonProcessingException {
        Optional<User> user = userRepository.getUserByUsernameOrEmailAndStatus(param.getUsername(), 0);
        if (user.isEmpty()) return GenerateResponse.notFound("User not found", null);
        else if (!encoder.matches(param.getPassword(), user.get().getUserPass())) return GenerateResponse.badRequest("password not matched", null);
        return GenerateResponse.success("login success", jwtUtil.generate(user.get(), "ACCESS"));
    }

    @Transactional
    public ResponseEntity<String> forgotPassword(String username) throws JsonProcessingException {
        Optional<User> user = userRepository.getUserByUsernameOrEmailAndStatus(username, 0);
        if (user.isEmpty()) return GenerateResponse.notFound("User not found", null);
        Optional<LookupMail> mail = lookupMailRepository.findLookupMailByLookupMailTopic("url-reset-password");
        return GenerateResponse.success("Please check email for reset your password, link just active on 5 minutes", null);
    }

    @Transactional
    public ResponseEntity<String> resetPassword(UserRequestResetPassword params, String token) throws JsonProcessingException {
        if (!params.getNewPassword().equals(params.getConfirmNewPassword())) return GenerateResponse.badRequest("Password and confirmation password not matched", null);
        else if (new Date().after(jwtUtil.getAllClaimsFromToken(token).getExpiration())) return GenerateResponse.badRequest("Token expired, please request forgot password again if you want to reset your password", null);
        Optional<User> user = userRepository.getUserByUsernameOrEmailAndStatus(jwtUtil.getUsernameFromToken(token), 0);
        if (user.isEmpty()) return GenerateResponse.notFound("User not found", null);
        user.get().setUserPass(params.getNewPassword());
        user.get().setUpdatedBy(user.get().getUserId());
        user.get().setUpdatedDate(new Date());
        userRepository.save(user.get());
        return GenerateResponse.success("Please check email for reset your password, link just active on 5 minutes", null);
    }

}
