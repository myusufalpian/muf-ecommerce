package com.muf.springjpaimplement.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.model.User;
import com.muf.springjpaimplement.model.UserOtp;
import com.muf.springjpaimplement.repository.UserOtpRepository;
import com.muf.springjpaimplement.repository.UserRepository;
import com.muf.springjpaimplement.util.GenerateResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserOtpService {

    private final UserOtpRepository userOtpRepository;
    private final UserRepository userRepository;

    @Transactional
    public ResponseEntity<String> checkOtp(String otp) throws JsonProcessingException {
        Optional<UserOtp> userOtp = userOtpRepository.findUserOtpByUserOtpCodeAndUserOtpStatus(otp, 0);
        if(!userOtp.isPresent()) {
            return GenerateResponse.notFound("User otp not found", null);
        } else if(userOtp.get().getUserOtpExp().before(new Date())) {
            userActivate(2, 1, userOtp.get());
            return GenerateResponse.badRequest("Otp was expired", null);
        }
        userActivate(1, 0, userOtp.get());
        return GenerateResponse.success("User activated success", null);
    }

    private void userActivate(Integer otpStatus, Integer userStatus, UserOtp userOtp) {
        userOtp.setUserOtpStatus(otpStatus);
        userOtp.setUpdatedBy(userOtp.getUserId());
        userOtp.setUpdatedDate(new Date());
        userOtpRepository.save(userOtp);

        Optional<User> user = userRepository.findById(userOtp.getUserId());
        user.get().setUserStatus(userStatus);
        user.get().setUpdatedDate(new Date());
        user.get().setUpdatedBy(user.get().getUserId());
        userRepository.save(user.get());
    }

}
