package com.muf.springjpaimplement.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.config.JwtUtil;
import com.muf.springjpaimplement.dto.MerchantCategoryDetail;
import com.muf.springjpaimplement.dto.NewMerchantCategotyRequest;
import com.muf.springjpaimplement.model.MerchantCategory;
import com.muf.springjpaimplement.model.User;
import com.muf.springjpaimplement.repository.MerchantCategoryRepository;
import com.muf.springjpaimplement.repository.UserRepository;
import com.muf.springjpaimplement.util.GenerateResponse;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MerchantCategoryService {

    private final MerchantCategoryRepository merchantCategoryRepository;
    private final UserRepository userRepository;
    private final ModelMapper mapper;
    private final JwtUtil jwtUtil;

    public ResponseEntity<String> getAllMerchantCategory() throws JsonProcessingException{
        Collection<MerchantCategory> merchantCategories = merchantCategoryRepository.findAll();
        return GenerateResponse.success("Get merchant success", merchantCategories.stream()
                .map(x -> mapper.map(x, MerchantCategoryDetail.class))
                .collect(Collectors.toList()));
    }

    public ResponseEntity<String> addNewMerchantCategory(NewMerchantCategotyRequest param, String token) throws JsonProcessingException {
        Optional<User> user = userRepository.getUserByUsernameOrEmailAndStatus(jwtUtil.validateJwt(token), 0);
        if (user.isEmpty()) return GenerateResponse.notFound("User not found", null);
        MerchantCategory merchantCategory = mapper.map(param, MerchantCategory.class);
        merchantCategory.setMerchantCategoryUuid(UUID.randomUUID().toString());
        merchantCategory.setCreatedBy(user.get().getUserId());
        merchantCategoryRepository.save(merchantCategory);
        return GenerateResponse.created("add new merchant category success", null);
    }

    public ResponseEntity<String> updateMerchantCategory(MerchantCategoryDetail param, String token) throws JsonProcessingException {
        Optional<User> user = userRepository.getUserByUsernameOrEmailAndStatus(jwtUtil.validateJwt(token), 0);
        if (user.isEmpty()) return GenerateResponse.notFound("User not found", null);
        Optional<MerchantCategory> merchantCategory = merchantCategoryRepository.findMerchantCategoryByMerchantCategoryUuid(param.getMerchantCategoryUuid());
        if (merchantCategory.isEmpty()) return GenerateResponse.notFound("Merchant category not found", null);
        merchantCategory.get().setMerchantCategoryName(param.getMerchantCategoryName());
        merchantCategory.get().setUpdatedBy(user.get().getUserId());
        merchantCategory.get().setUpdatedDate(new Date());
        merchantCategoryRepository.save(merchantCategory.get());
        return GenerateResponse.success("Update merchant category success", null);
    }

}
