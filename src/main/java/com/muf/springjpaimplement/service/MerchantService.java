package com.muf.springjpaimplement.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.config.JwtUtil;
import com.muf.springjpaimplement.dto.GetMerchantDetailto;
import com.muf.springjpaimplement.dto.NewMerchantRequest;
import com.muf.springjpaimplement.model.Merchant;
import com.muf.springjpaimplement.model.MerchantCategory;
import com.muf.springjpaimplement.model.MerchantDetail;
import com.muf.springjpaimplement.repository.MerchantCategoryRepository;
import com.muf.springjpaimplement.repository.MerchantDetailRepository;
import com.muf.springjpaimplement.repository.MerchantRepository;
import com.muf.springjpaimplement.util.GenerateResponse;
import com.muf.springjpaimplement.util.Helper;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class MerchantService {

    private final MerchantCategoryRepository merchantCategoryRepository;

    private final MerchantDetailRepository merchantDetailRepository;

    private final MerchantRepository merchantRepository;

    private final Helper helper;

    private final JwtUtil jwtUtil;

    private final ModelMapper mapper;

    @Transactional
    public ResponseEntity<String> addNewMerchant(NewMerchantRequest merchantRequest, String token) throws JsonProcessingException {
        Integer userId = helper.getUserId(jwtUtil.validateJwt(token));
        if (userId == 0) return GenerateResponse.notFound("user not found", null);
        Optional<MerchantCategory> merchantCategory = merchantCategoryRepository.findMerchantCategoryByMerchantCategoryUuid(merchantRequest.getMerchantCategoryUuid());
        if (merchantCategory.isEmpty()) return GenerateResponse.notFound("merchant category not found!", null);
        Optional<Merchant> checkUserMerchant = merchantRepository.findMerchantByCreatedBy(userId);
        if (checkUserMerchant.isPresent()) return GenerateResponse.badRequest("User already have merchant", null);
        Merchant merchant = new Merchant();
        merchant.setMerchantUuid(UUID.randomUUID().toString());
        merchant.setMerchantCategoryId(merchantCategory.get().getMerchantCategoryId());
        merchant.setCreatedBy(userId);
        merchantRepository.save(merchant);
        MerchantDetail merchantDetail = mapper.map(merchantRequest, MerchantDetail.class);
        merchantDetail.setMerchantDetailUuid(UUID.randomUUID().toString());
        merchantDetail.setMerchantId(merchant.getMerchantId());
        merchantDetail.setCreatedBy(userId);
        merchantDetail.setCreatedDate(new Date());
        merchantDetail.setMerchantDetailName(merchantDetail.getMerchantDetailName());
        merchantDetailRepository.save(merchantDetail);
        return GenerateResponse.created("Add new merchant success", null);
    }

    public ResponseEntity<String> getDetailMerchantViewByCreatedBy(String token) throws JsonProcessingException {
        Integer userId = helper.getUserId(jwtUtil.validateJwt(token));
        if (userId == 0) return GenerateResponse.notFound("user not found", null);
        Optional<GetMerchantDetailto> getMerchantDetail = merchantDetailRepository.getMerchantDetailByUserId(userId);
        if (getMerchantDetail.isEmpty()) return GenerateResponse.notFound("user don't have any merchant", null);
        return GenerateResponse.success("Add new merchant success", getMerchantDetail.get());
    }

    public ResponseEntity<String> getDetailMerchantByUserView(String merchantUuid) throws JsonProcessingException {
        Optional<GetMerchantDetailto> getMerchantDetail = merchantDetailRepository.getMerchantDetailByMerchantUuid(merchantUuid);
        if (getMerchantDetail.isEmpty()) return GenerateResponse.notFound("merchant is not found", null);
        return GenerateResponse.success("Add new merchant success", getMerchantDetail.get());
    }

}
