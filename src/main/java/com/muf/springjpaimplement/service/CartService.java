package com.muf.springjpaimplement.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.config.JwtUtil;
import com.muf.springjpaimplement.dto.ListItemCart;
import com.muf.springjpaimplement.dto.ListItemUserOnCart;
import com.muf.springjpaimplement.dto.RequestItemCart;
import com.muf.springjpaimplement.dto.ResponseFinalItemCart;
import com.muf.springjpaimplement.model.Cart;
import com.muf.springjpaimplement.model.CartItem;
import com.muf.springjpaimplement.model.Discount;
import com.muf.springjpaimplement.model.Item;
import com.muf.springjpaimplement.repository.CartItemRepository;
import com.muf.springjpaimplement.repository.CartRepository;
import com.muf.springjpaimplement.repository.DiscountRepository;
import com.muf.springjpaimplement.repository.ItemRepository;
import com.muf.springjpaimplement.util.GenerateResponse;
import com.muf.springjpaimplement.util.Helper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class CartService {

    private final CartRepository cartRepository;

    private final CartItemRepository cartItemRepository;

    private final ItemRepository itemRepository;

    private final DiscountRepository discountRepository;

    private final JwtUtil jwtUtil;

    private final Helper helper;

    public ResponseEntity<String> addItemToUserCart(String token, RequestItemCart params) throws JsonProcessingException {
        String username = jwtUtil.validateJwt(token);
        if (username.equals("1")) return GenerateResponse.unauthorized("Token was invalid", null);
        else if (username.equals("2")) return GenerateResponse.unauthenticated("Token was expired", null);
        Integer userId = helper.getUserId(username);
        if (userId == 0) return GenerateResponse.success("user not found", null);
        Optional<Cart> checkUserCart = cartRepository.findCartByCreatedByAndCartStatus(userId, 0);
        if (checkUserCart.isEmpty()) {
            Cart cart = new Cart();
            cart.setCartUuid(UUID.randomUUID().toString());
            cart.setCartStatus(0);
            cart.setCreatedBy(userId);
            cart.setCreatedDate(new Date());
            cartRepository.save(cart);
            for (ListItemCart x: params.getItemCarts()) {
                Optional<Item> item = itemRepository.findItemByItemUuidAndItemStatus(x.getItemUuid(), 0);
                if (item.isEmpty()) return GenerateResponse.notFound("Item not found", null);
                else if (item.get().getItemStock() < x.getItemQty()) return GenerateResponse.badRequest("Item not enough, Quantity Item : " + item.get().getItemStock() , null);
                generateItemCart(checkUserCart.get(), item.get(), x.getItemQty(), userId);
            }
        }else {
            for (ListItemCart x: params.getItemCarts()) {
                Optional<Item> item = itemRepository.findItemByItemUuidAndItemStatus(x.getItemUuid(), 0);
                if (item.isEmpty()) return GenerateResponse.notFound("Item not found", null);
                else if (item.get().getItemStock() < x.getItemQty()) return GenerateResponse.badRequest("Item not enough, Quantity Item : " + item.get().getItemStock() , null);
                Optional<CartItem> checkItem = cartItemRepository.findByItemIdAndCartIdAndCartItemStatus(item.get().getItemId(), checkUserCart.get().getCartId(), 0);
                if (checkItem.isEmpty()) {
                    generateItemCart(checkUserCart.get(), item.get(), x.getItemQty(), userId);
                } else {
                    if ((checkItem.get().getItemQty() + x.getItemQty()) > item.get().getItemStock()) {
                        return GenerateResponse.badRequest("Item not enough, Quantity Item : " + item.get().getItemStock(), null);
                    }
                    checkItem.get().setItemQty(checkItem.get().getItemQty() + x.getItemQty());
                    checkItem.get().setUpdatedBy(userId);
                    checkItem.get().setUpdatedDate(new Date());
                    cartItemRepository.save(checkItem.get());
                }
            }
        }
        return GenerateResponse.success("Add item to cart success", null);
    }

    public ResponseEntity<String> detailCart(String token) throws JsonProcessingException {
        String username = jwtUtil.validateJwt(token);
        if (username.equals("1")) return GenerateResponse.unauthorized("Token was invalid", null);
        else if (username.equals("2")) return GenerateResponse.unauthenticated("Token was expired", null);
        Integer userId = helper.getUserId(username);
        if (userId == 0) return GenerateResponse.success("user not found", null);
        List<ListItemUserOnCart> itemCarts = cartItemRepository.findCartItemByUserId(userId, 0);
        if (itemCarts.isEmpty()) return GenerateResponse.success("Cart is empty", null);
        return GenerateResponse.success("Get data success", itemCarts);
    }

    public ResponseEntity<String> deleteItemOnCart(String cartUuid, String itemUuid) throws JsonProcessingException {
        Optional<CartItem> cartItem = cartItemRepository.findByItemUuidAndCartUuidAndIsdel(itemUuid, cartUuid, 0);
        if (cartItem.isEmpty()) return GenerateResponse.notFound("Item not found in this cart", null);
        cartItem.get().setCartItemStatus(1);
        cartItemRepository.save(cartItem.get());
        return GenerateResponse.success("Delete item from cart success", null);
    }

    public ResponseEntity<String> reduceItemOnCart(String token, String cartUuid, String itemUuid, Integer amount) throws JsonProcessingException {
        String username = jwtUtil.validateJwt(token);
        if (username.equals("1")) return GenerateResponse.unauthorized("Token was invalid", null);
        else if (username.equals("2")) return GenerateResponse.unauthenticated("Token was expired", null);
        Integer userId = helper.getUserId(username);
        if (userId == 0) return GenerateResponse.success("user not found", null);
        Optional<CartItem> itemCart = cartItemRepository.findByItemUuidAndCartUuidAndIsdel(itemUuid, cartUuid, 0);
        if (itemCart.isEmpty()) return GenerateResponse.notFound("Item not found in this cart", null);
        else if (itemCart.get().getItemQty() < amount) {
            return GenerateResponse.badRequest("Item not enough, Quantity Item : " + itemCart.get().getItemQty(), null);
        } else {
            if (itemCart.get().getItemQty() > amount) {
                itemCart.get().setItemQty(itemCart.get().getItemQty() - amount);
                itemCart.get().setUpdatedBy(userId);
                itemCart.get().setUpdatedDate(new Date());
                cartItemRepository.save(itemCart.get());
                return GenerateResponse.success("Reduce item from cart success", null);
            } else {
                itemCart.get().setCartItemStatus(1);
                cartItemRepository.save(itemCart.get());
                return GenerateResponse.success("Delete item from cart success", null);
            }
        }
    }

    public ResponseEntity<String> calculateItemOnCart(String token, RequestItemCart param, String disc) throws JsonProcessingException {
        String username = jwtUtil.validateJwt(token);
        if (username.equals("1")) return GenerateResponse.unauthorized("Token was invalid", null);
        else if (username.equals("2")) return GenerateResponse.unauthenticated("Token was expired", null);
        Integer userId = helper.getUserId(username);
        if (userId == 0) return GenerateResponse.success("user not found", null);
        Optional<Cart> cart = cartRepository.findCartByCreatedByAndCartStatus(userId, 0);
        if (cart.isEmpty()) return GenerateResponse.notFound("Cart not found", null);
        else if (param.getItemCarts().isEmpty()) return GenerateResponse.badRequest("Please select item", null);
        HashMap finalCart = new HashMap<>();
        Integer price = 0;
        List<ResponseFinalItemCart> items = new ArrayList<>();
        for (ListItemCart x : param.getItemCarts()) {
            ResponseFinalItemCart tempItem = new ResponseFinalItemCart();
            Optional<Item> item = itemRepository.findItemByItemUuidAndItemStatus(x.getItemUuid(), 0);
            if (item.isEmpty()) return GenerateResponse.notFound("Item not found", null);
            Integer tempPrice = x.getItemQty() * item.get().getItemPrice();
            tempItem.setItemName(item.get().getItemName());
            tempItem.setItemUuid(item.get().getItemUuid());
            tempItem.setItemPrice(item.get().getItemPrice());
            tempItem.setItemQty(x.getItemQty());
            tempItem.setItemTotalPrice(tempPrice);
            items.add(tempItem);
            price += tempPrice;
        }
        if ( !disc.equals("") || disc != null ) {
            Optional<Discount> discount = discountRepository.findDiscountByDiscountCodeAndDiscountStatus(disc, 0);
            if (discount.isEmpty()) {
                return GenerateResponse.badRequest("Discount code was invalid", null);
            } else if (discount.get().getDiscountQty() < 1) {
                return GenerateResponse.badRequest("Discount amount not enough", null);
            } else {
                finalCart.put("discountRate", discount.get().getDiscountRate()+"%");
                Integer discPrice = (price * discount.get().getDiscountRate() / 100);
                finalCart.put("discountPrice", discPrice > discount.get().getDiscountMaxRate() ? discount.get().getDiscountMaxRate() : discPrice);
                finalCart.put("summaryPrice", discPrice > discount.get().getDiscountMaxRate() ? price - discount.get().getDiscountMaxRate() : price - discPrice);
            }
        }
        finalCart.put("items", items);
        finalCart.put("totalPrices", price);
        return GenerateResponse.success("Success calculate items", finalCart);
    }


    private void generateItemCart(Cart cart, Item item, Integer itemQty, Integer userId) {
        CartItem cartItem = new CartItem();
        cartItem.setCartItemUuid(UUID.randomUUID().toString());
        cartItem.setCartId(cart.getCartId());
        cartItem.setItemId(item.getItemId());
        cartItem.setItemQty(itemQty);
        cartItem.setCartItemStatus(0);
        cartItem.setCreatedBy(userId);
        cartItem.setCreatedDate(new Date());
        cartItemRepository.save(cartItem);
    }
}
