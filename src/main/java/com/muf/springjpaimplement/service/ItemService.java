package com.muf.springjpaimplement.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.config.JwtUtil;
import com.muf.springjpaimplement.dto.ItemListDetail;
import com.muf.springjpaimplement.dto.MerchantCategoryDetail;
import com.muf.springjpaimplement.dto.PagingItemList;
import com.muf.springjpaimplement.dto.RequestNewItem;
import com.muf.springjpaimplement.model.Category;
import com.muf.springjpaimplement.model.Item;
import com.muf.springjpaimplement.model.Merchant;
import com.muf.springjpaimplement.repository.CategotyRepository;
import com.muf.springjpaimplement.repository.ItemRepository;
import com.muf.springjpaimplement.repository.MerchantRepository;
import com.muf.springjpaimplement.util.GenerateResponse;
import com.muf.springjpaimplement.util.Helper;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
@RequiredArgsConstructor
public class ItemService {

    private final ItemRepository itemRepository;

    private final CategotyRepository categotyRepository;

    private final MerchantRepository merchantRepository;

    private final Helper helper;

    private final JwtUtil jwtUtil;

    private final ModelMapper mapper;

    @Transactional
    public ResponseEntity<String> addNewItem(String token, RequestNewItem param) throws JsonProcessingException {
        String username = jwtUtil.validateJwt(token);
        if (username.equals("1")) return GenerateResponse.unauthorized("Token invalid", null);
        else if (username.equals("2")) return GenerateResponse.unauthenticated("Token was expired", null);
        Integer userId = helper.getUserId(username);
        if (userId == null) return GenerateResponse.notFound("user not found", null);
        Optional<Category> category = categotyRepository.findCategoriesByCategoryUuidAndCategoryStatus(param.getCategoryUuid(), 0);
        Optional<Merchant> merchant = merchantRepository.findMerchantByMerchantUuid(param.getMerchantUuid());
        if (category.isEmpty()) return GenerateResponse.notFound("category not found", null);
        else if (merchant.isEmpty()) return GenerateResponse.notFound("merchant not found", null);
        Item item = mapper.map(param, Item.class);
        item.setMerchantId(merchant.get().getMerchantId());
        item.setCategoryId(category.get().getCategoryId());
        item.setItemUuid(UUID.randomUUID().toString());
        item.setCreatedBy(userId);
        item.setCreatedDate(new Date());
        itemRepository.save(item);
        return GenerateResponse.created("add new item success", null);
    }

    @Transactional
    public ResponseEntity<String> updateItem(String token, String itemUuid, RequestNewItem param) throws JsonProcessingException {
        String username = jwtUtil.validateJwt(token);
        if (username.equals("1")) return GenerateResponse.unauthorized("Token invalid", null);
        else if (username.equals("2")) return GenerateResponse.unauthenticated("Token was expired", null);
        Integer userId = helper.getUserId(username);
        if (userId == 0) return GenerateResponse.notFound("user not found", null);
        Optional<Category> category = categotyRepository.findCategoriesByCategoryUuidAndCategoryStatus(param.getCategoryUuid(), 0);
        Optional<Merchant> merchant = merchantRepository.findMerchantByMerchantUuid(param.getMerchantUuid());
        if (category.isEmpty()) return GenerateResponse.notFound("category not found", null);
        else if (merchant.isEmpty()) return GenerateResponse.notFound("merchant not found", null);
        Optional<Item> item = itemRepository.findItemByItemUuidAndItemStatus(itemUuid, 0);
        if (item.isEmpty()) return GenerateResponse.notFound("item not found", null);
        item.get().setMerchantId(merchant.get().getMerchantId());
        item.get().setCategoryId(category.get().getCategoryId());
        item.get().setItemName(param.getItemName());
        item.get().setItemStock(param.getItemStock());
        item.get().setItemPrice(param.getItemPrice());
        item.get().setUpdatedBy(userId);
        item.get().setUpdatedDate(new Date());
        itemRepository.save(item.get());
        return GenerateResponse.success("add new item success", null);
    }

    public ResponseEntity<String> getItemByCategory(String categoryUuid, Pageable pageable) throws JsonProcessingException {
        PagingItemList pagingItem = new PagingItemList();
        Sort sort = pageable.getSort();
        List<Sort.Order> newSortOrder = new ArrayList<>();
        for (Sort.Order order : sort) {
            if (order.getProperty().equalsIgnoreCase("itemName")){
                newSortOrder.add(new Sort.Order(order.getDirection(), order.getProperty()));
            }else{
                newSortOrder.add(new Sort.Order(order.getDirection(), order.getProperty()));
            }
        }
        Sort newSort = Sort.by(newSortOrder);
        Pageable newPageable = PageRequest.of(
                pageable.getPageNumber(),
                pageable.getPageSize(), newSort);
        Page<ItemListDetail> itemList = itemRepository.findItemByCategoryUuidAndItemStatus(categoryUuid, 0, newPageable);
        HashMap<String, Object> pages = new HashMap<>();
        pages.put("size", newPageable.getPageSize());
        pages.put("totalElements", itemList.getTotalElements());
        pages.put("totalPages", itemList.getTotalPages());
        pages.put("page", newPageable.getPageNumber());
        pagingItem.setPages(pages);
        pagingItem.setData(itemList.getContent());
        return GenerateResponse.success("get data success", pagingItem);
    }

    public ResponseEntity<String> getDetailItem(String itemUuid) throws JsonProcessingException {
        Optional<Item> item = itemRepository.findItemByItemUuidAndItemStatus(itemUuid, 0);
        return item.isEmpty() ? GenerateResponse.notFound("item not found", null) : GenerateResponse.success("get data success", mapper.map(item, Item.class));
    }

    public ResponseEntity<String> getItemBySearch(String search, Pageable pageable) throws JsonProcessingException {
        PagingItemList pagingItem = new PagingItemList();
        Sort sort = pageable.getSort();
        List<Sort.Order> newSortOrder = new ArrayList<>();
        for (Sort.Order order : sort) {
            if (order.getProperty().equalsIgnoreCase("itemName")){
                newSortOrder.add(new Sort.Order(order.getDirection(), order.getProperty()));
            }else{
                newSortOrder.add(new Sort.Order(order.getDirection(), order.getProperty()));
            }
        }
        Sort newSort = Sort.by(newSortOrder);
        Pageable newPageable = PageRequest.of(
                pageable.getPageNumber(),
                pageable.getPageSize(), newSort);
        Page<ItemListDetail> itemList = itemRepository.findItemByItemStatus(search, 0, newPageable);
        HashMap<String, Object> pages = new HashMap<>();
        pages.put("size", newPageable.getPageSize());
        pages.put("totalElements", itemList.getTotalElements());
        pages.put("totalPages", itemList.getTotalPages());
        pages.put("page", newPageable.getPageNumber());
        pagingItem.setPages(pages);
        pagingItem.setData(itemList.getContent());
        return GenerateResponse.success("get data success", pagingItem);
    }

}
