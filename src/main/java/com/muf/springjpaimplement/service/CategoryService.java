package com.muf.springjpaimplement.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.dto.CategoryDetail;
import com.muf.springjpaimplement.model.Category;
import com.muf.springjpaimplement.repository.CategotyRepository;
import com.muf.springjpaimplement.util.GenerateResponse;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategotyRepository categotyRepository;

    private final ModelMapper mapper;

    @Transactional
    public ResponseEntity<String> addNewCategory(String categoryName) throws JsonProcessingException {
        Category category = new Category();
        category.setCategoryUuid(UUID.randomUUID().toString());
        category.setCreatedBy(0);
        category.setCreatedDate(new Date());
        category.setCategoryName(categoryName);
        categotyRepository.save(category);
        return GenerateResponse.created("add new category success", null);
    }

    @Transactional
    public ResponseEntity<String> updateCategory(String categoryUuid, String categoryName) throws JsonProcessingException {
        Optional<Category> category = categotyRepository.findCategoriesByCategoryUuidAndCategoryStatus(categoryUuid, 0);
        category.get().setCategoryUuid(UUID.randomUUID().toString());
        category.get().setUpdatedBy(0);
        category.get().setUpdatedDate(new Date());
        category.get().setCategoryName(categoryName);
        categotyRepository.save(category.get());
        return GenerateResponse.success("update category success", null);
    }

    public ResponseEntity<String> detailCategory(String categoryUuid) throws JsonProcessingException {
        Optional<Category> category = categotyRepository.findCategoriesByCategoryUuidAndCategoryStatus(categoryUuid, 0);
        return category.isEmpty() ? GenerateResponse.notFound("Get category failed", null) : GenerateResponse.success("get data category success", mapper.map(category.get(), CategoryDetail.class));
    }

    public ResponseEntity<String> listCategory() throws JsonProcessingException {
        Collection<Category> category = categotyRepository.findCategoriesByCategoryStatus(0);
        return GenerateResponse.success("get data category success", category.stream()
                .map(x -> mapper.map(x, CategoryDetail.class))
                .collect(Collectors.toList()));
    }

}
