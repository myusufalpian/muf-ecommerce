package com.muf.springjpaimplement.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.muf.springjpaimplement.constant.ConstantSQL;
import com.muf.springjpaimplement.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
    
    @Query(nativeQuery = true, value = ConstantSQL.getUserByUsernameOrEmailAndStatus)
    Optional<User> getUserByUsernameOrEmailAndStatus(String username, Integer status);

    @Query(nativeQuery = true, value = ConstantSQL.getUserByUsernameOrEmail)
    Optional<User> getUserByUsernameOrEmail(String username);

}
