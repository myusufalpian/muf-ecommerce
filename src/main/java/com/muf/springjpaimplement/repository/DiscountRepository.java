package com.muf.springjpaimplement.repository;

import com.muf.springjpaimplement.model.Discount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DiscountRepository extends JpaRepository<Discount, Integer> {
    Optional<Discount> findDiscountByDiscountCodeAndDiscountStatus(String discountCode, Integer discountStatus);

    Optional<Discount> findDiscountByDiscountUuidAndDiscountStatus(String discountUuid, Integer discountStatus);

    List<Discount> findDiscountByCreatedByAndDiscountStatus(Integer createdBy, Integer discountStatus);

}
