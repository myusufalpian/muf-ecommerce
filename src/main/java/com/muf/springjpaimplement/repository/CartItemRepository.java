package com.muf.springjpaimplement.repository;

import com.muf.springjpaimplement.constant.ConstantSQL;
import com.muf.springjpaimplement.dto.ListItemUserOnCart;
import com.muf.springjpaimplement.model.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CartItemRepository extends JpaRepository<CartItem, Integer> {
    Optional<CartItem> findByItemIdAndCartIdAndCartItemStatus(Integer itemId, Integer cartId, Integer cartItemStatus);

    @Query(nativeQuery = true, value = ConstantSQL.getItemListOnCart)
    List<ListItemUserOnCart> findCartItemByUserId(Integer createdBy, Integer cartItemStatus);

    @Query(nativeQuery = true, value = ConstantSQL.getItemCartItemUuidAndCartUuidAndIsdel)
    Optional<CartItem> findByItemUuidAndCartUuidAndIsdel(String itemUuid, String cartUuid, Integer isdel);
}
