package com.muf.springjpaimplement.repository;

import com.muf.springjpaimplement.model.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MerchantRepository extends JpaRepository<Merchant, Integer> {
    Optional<Merchant> findMerchantByCreatedBy(Integer createdBy);
    Optional<Merchant> findMerchantByMerchantUuid(String merchantUuid);
}
