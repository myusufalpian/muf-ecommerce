package com.muf.springjpaimplement.repository;

import com.muf.springjpaimplement.model.MerchantCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MerchantCategoryRepository extends JpaRepository<MerchantCategory, Integer> {
    Optional<MerchantCategory> findMerchantCategoryByMerchantCategoryUuid(String merchantCategoryUuid);
    Optional<MerchantCategory> findMerchantCategoryByMerchantCategoryName(String merchantCategoryName);
}
