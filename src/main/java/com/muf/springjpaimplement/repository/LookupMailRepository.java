package com.muf.springjpaimplement.repository;

import com.muf.springjpaimplement.model.LookupMail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LookupMailRepository extends JpaRepository<LookupMail, Integer> {
    Optional<LookupMail> findLookupMailByLookupMailTopic(String topic);
}
