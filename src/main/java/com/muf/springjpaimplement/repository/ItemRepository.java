package com.muf.springjpaimplement.repository;

import com.muf.springjpaimplement.constant.ConstantSQL;
import com.muf.springjpaimplement.dto.ItemListDetail;
import com.muf.springjpaimplement.model.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ItemRepository extends JpaRepository<Item, Integer> {
    Optional<Item> findItemByItemUuidAndItemStatus(String itemUuid, Integer itemStatus);
    @Query(nativeQuery = true, value = ConstantSQL.getItemByCategoryUuid, countQuery = ConstantSQL.getCountItemByCategoryUuid)
    Page<ItemListDetail> findItemByCategoryUuidAndItemStatus(String categoryUuid, Integer itemStatus, Pageable pageable);

    @Query(nativeQuery = true, value = ConstantSQL.getItemBySearch, countQuery = ConstantSQL.getCountItemBySearch)
    Page<ItemListDetail> findItemByItemStatus(String search, Integer itemStatus, Pageable pageable);

}
