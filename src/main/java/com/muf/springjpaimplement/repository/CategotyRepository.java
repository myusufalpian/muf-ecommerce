package com.muf.springjpaimplement.repository;

import com.muf.springjpaimplement.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface CategotyRepository extends JpaRepository<Category, Integer> {
    Optional<Category> findCategoriesByCategoryUuidAndCategoryStatus(String categoryUuid, Integer categoryStatus);

    Collection<Category> findCategoriesByCategoryStatus(Integer categoryStatus);

}
