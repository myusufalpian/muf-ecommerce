package com.muf.springjpaimplement.repository;

import com.muf.springjpaimplement.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CartRepository extends JpaRepository<Cart, Integer> {

    Optional<Cart> findCartByCreatedByAndCartStatus(Integer createdBy, Integer cartStatus);

}
