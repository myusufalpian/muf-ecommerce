package com.muf.springjpaimplement.repository;

import com.muf.springjpaimplement.constant.ConstantSQL;
import com.muf.springjpaimplement.dto.GetMerchantDetailto;
import com.muf.springjpaimplement.model.MerchantDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface MerchantDetailRepository extends JpaRepository<MerchantDetail, Integer> {

    @Query(nativeQuery = true, value = ConstantSQL.getMerchantDetailByUserId)
    Optional<GetMerchantDetailto> getMerchantDetailByUserId(Integer userId);

    @Query(nativeQuery = true, value = ConstantSQL.getGetMerchantDetailByMerchantUuid)
    Optional<GetMerchantDetailto> getMerchantDetailByMerchantUuid(String merchantUuid);

}
