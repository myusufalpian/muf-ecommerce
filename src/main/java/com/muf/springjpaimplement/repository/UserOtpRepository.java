package com.muf.springjpaimplement.repository;

import com.muf.springjpaimplement.model.UserOtp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserOtpRepository extends JpaRepository<UserOtp, Integer> {
    Optional<UserOtp> findUserOtpByUserOtpCodeAndUserOtpStatus(String userOtpCode, Integer userOtpStatus);
}
