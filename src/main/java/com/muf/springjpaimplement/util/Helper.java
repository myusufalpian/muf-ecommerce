package com.muf.springjpaimplement.util;

import com.muf.springjpaimplement.model.User;
import com.muf.springjpaimplement.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.Random;

@Component
@RequiredArgsConstructor
public class Helper {

    private final UserRepository userRepository;

    public static String generateOTP(Integer length) {
        StringBuilder otp = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            int digit = random.nextInt(10); // Generate a random digit between 0 and 9
            otp.append(digit);
        }
        return otp.toString();
    }

    public static Date addTimes(Date date, String type, Integer times) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        if (type.equalsIgnoreCase("minutes")) {
            calendar.add(Calendar.MINUTE, times);
        } else if (type.equalsIgnoreCase("hours")) {
            calendar.add(Calendar.HOUR, times);
        } else if (type.equalsIgnoreCase("days")) {
            calendar.add(Calendar.DAY_OF_MONTH, times);
        }
        return calendar.getTime();
    }

    public Integer getUserId(String username) {
        Optional<User> user = userRepository.getUserByUsernameOrEmailAndStatus(username, 0);
        return user.isEmpty() ? 0 : user.get().getUserId();
    }
}
