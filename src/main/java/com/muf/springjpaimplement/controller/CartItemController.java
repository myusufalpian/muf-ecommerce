package com.muf.springjpaimplement.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.dto.RequestItemCart;
import com.muf.springjpaimplement.service.CartService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/cart", produces = MediaType.APPLICATION_JSON_VALUE)
public class CartItemController {

    private final CartService cartService;

    @PostMapping("/addNewItemToCart")
    public ResponseEntity<String> addNewItemToCart(@RequestHeader("Authorization") String token, @RequestBody RequestItemCart param) throws JsonProcessingException {
        return cartService.addItemToUserCart(token, param);
    }

    @GetMapping("/detailCart")
    public ResponseEntity<String> detailCart(@RequestHeader("Authorization") String token) throws JsonProcessingException {
        return cartService.detailCart(token);
    }

    @DeleteMapping("/deleteItemOnCart/{cartUuid}/{itemUuid}")
    public ResponseEntity<String> addNewItemToCart(@RequestHeader("Authorization") String token, @PathVariable String cartUuid, @PathVariable String itemUuid) throws JsonProcessingException {
        return cartService.deleteItemOnCart(cartUuid, itemUuid);
    }

    @PutMapping("/calculateItemOnCart")
    public ResponseEntity<String> calculateItemOnCart(@RequestHeader("Authorization") String token, @RequestBody RequestItemCart param, @RequestParam String disc) throws JsonProcessingException {
        return cartService.calculateItemOnCart(token, param, disc);
    }

}
