package com.muf.springjpaimplement.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.dto.RequestDiscount;
import com.muf.springjpaimplement.dto.RequestNewDiscount;
import com.muf.springjpaimplement.service.DiscountService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/discount", produces = MediaType.APPLICATION_JSON_VALUE)
public class DiscountController {
    private final DiscountService discountService;

    @PostMapping("/addNewDiscount")
    public ResponseEntity<String> addNewDiscount(@RequestHeader("Authorization") String token, @RequestBody RequestNewDiscount param) throws JsonProcessingException {
        return discountService.addDiscount(token, param);
    }

    @PutMapping("/updateDiscount/{discountUuid}")
    public ResponseEntity<String> updateDiscount(@RequestHeader("Authorization") String token, @PathVariable String discountUuid, @RequestBody RequestDiscount param) throws JsonProcessingException {
        return discountService.updateDiscount(token, discountUuid, param);
    }

    @GetMapping("/listDiscount")
    public ResponseEntity<String> listDiscount(@RequestHeader("Authorization") String token) throws JsonProcessingException {
        return discountService.listDiscount(token);
    }

    @GetMapping("/detailDiscount/{discountUuid}")
    public ResponseEntity<String> detailDiscount(@RequestHeader("Authorization") String token, @PathVariable String discountUuid) throws JsonProcessingException {
        return discountService.detailDiscount(discountUuid);
    }

    @DeleteMapping("deleteDiscount/{discountUuid}")
    public ResponseEntity<String> deleteDiscount(@RequestHeader("Authorization") String token, @PathVariable String discountUuid) throws JsonProcessingException {
        return discountService.deleteDiscount(discountUuid);
    }

}
