package com.muf.springjpaimplement.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.dto.MerchantCategoryDetail;
import com.muf.springjpaimplement.dto.NewMerchantCategotyRequest;
import com.muf.springjpaimplement.service.MerchantCategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/merchantCategory", produces = MediaType.APPLICATION_JSON_VALUE)
public class MerchantCategoryController {
    private final MerchantCategoryService merchantCategoryService;

    @GetMapping("/getAll")
    public ResponseEntity<String> getAllMerchantCategory(@RequestHeader(name = "Authorization") String token) throws JsonProcessingException {
        return merchantCategoryService.getAllMerchantCategory();
    }

    @PostMapping("/addNewMerchantCategory")
    public ResponseEntity<String> getAllMerchantCategory(@RequestHeader(name = "Authorization") String token, @RequestBody NewMerchantCategotyRequest param) throws JsonProcessingException {
        return merchantCategoryService.addNewMerchantCategory(param, token);
    }

    @PutMapping("/updateMerchantCategory")
    public ResponseEntity<String> updateMerchantCategory(@RequestHeader(name = "Authorization") String token, @RequestBody MerchantCategoryDetail param) throws JsonProcessingException {
        return merchantCategoryService.updateMerchantCategory(param, token);
    }

}
