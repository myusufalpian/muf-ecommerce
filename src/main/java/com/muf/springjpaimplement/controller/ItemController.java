package com.muf.springjpaimplement.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.dto.RequestNewItem;
import com.muf.springjpaimplement.service.ItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/item")
public class ItemController {

    private final ItemService itemService;

    @PostMapping("/addNewItem")
    public ResponseEntity<String> addNewItem(@RequestHeader(name = "Authorization") String token, @RequestBody RequestNewItem param) throws JsonProcessingException {
        return itemService.addNewItem(token, param);
    }

    @GetMapping("/getListItem")
    public ResponseEntity<String> getListItem(
        @RequestHeader(name = "Authorization") String token,
        @RequestParam(defaultValue = "", required = false, name = "search") String search,
        @PageableDefault(sort = {"itemUuid"}, direction = Sort.Direction.DESC, size = 30, page = 0 ) Pageable pageable
    ) throws JsonProcessingException {
        return itemService.getItemBySearch(search, pageable);
    }

    @PutMapping("/updateItem/{itemUuid}")
    public ResponseEntity<String> updateItem(@RequestHeader(name = "Authorization") String token, @RequestBody RequestNewItem param, @PathVariable String itemUuid) throws JsonProcessingException {
        return itemService.updateItem(token, itemUuid, param);
    }

    @GetMapping("/getItemByCategory/{categoryUuid}")
    public ResponseEntity<String> getItemByCategory(
            @RequestHeader(name = "Authorization") String token,
            @PathVariable String categoryUuid,
            @PageableDefault(sort = {"itemUuid"}, direction = Sort.Direction.DESC, size = 30, page = 0 ) Pageable pageable
    ) throws JsonProcessingException {
        return itemService.getItemByCategory(categoryUuid, pageable);
    }

}
