package com.muf.springjpaimplement.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/category", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;

    @GetMapping("/addNewCategory")
    public ResponseEntity<String> addNewCategory(@RequestHeader(name = "Authorization") String token, @RequestParam(name = "categoryName", required = true) String categoryName) throws JsonProcessingException {
        return categoryService.addNewCategory(categoryName);
    }

    @GetMapping("/updateCategory/{categoryUuid}")
    public ResponseEntity<String> updateCategory(@RequestHeader(name = "Authorization") String token,
             @RequestParam(name = "categoryName", required = true) String categoryName,
            @PathVariable String categoryUuid
    ) throws JsonProcessingException {
        return categoryService.updateCategory(categoryUuid, categoryName);
    }

    @GetMapping("/getListCategory")
    public ResponseEntity<String> getListCategory(@RequestHeader(name = "Authorization") String token) throws JsonProcessingException {
        return categoryService.listCategory();
    }

    @GetMapping("/getDetailCategory/{categoryUuid}")
    public ResponseEntity<String> getDetailCategory(@RequestHeader(name = "Authorization") String token, @PathVariable String categoryUuid) throws JsonProcessingException {
        return categoryService.detailCategory(categoryUuid);
    }


}
