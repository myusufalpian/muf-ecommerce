package com.muf.springjpaimplement.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.dto.UserRequestLogin;
import com.muf.springjpaimplement.dto.UserRequestRegister;
import com.muf.springjpaimplement.dto.UserRequestResetPassword;
import com.muf.springjpaimplement.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping(value = "/login")
    private ResponseEntity<String> login(@RequestBody UserRequestLogin params) throws JsonProcessingException {
        return authService.login(params);
    }

    @PostMapping(value = "/register")
    private ResponseEntity<String> register(@RequestBody UserRequestRegister params) throws JsonProcessingException {
        return authService.register(params);
    }

    @GetMapping(value = "/forgot")
    private ResponseEntity<String> forgot(@RequestParam(name = "username", required = true) String username) throws JsonProcessingException {
        return authService.forgotPassword(username);
    }

    @PostMapping(value = "/reset")
    private ResponseEntity<String> reset(@RequestBody UserRequestResetPassword param, @RequestParam(name = "token", required = true) String token) throws JsonProcessingException {
        return authService.resetPassword(param, token);
    }
}
