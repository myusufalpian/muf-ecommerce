package com.muf.springjpaimplement.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.muf.springjpaimplement.dto.NewMerchantRequest;
import com.muf.springjpaimplement.service.MerchantService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/merchant", produces = MediaType.APPLICATION_JSON_VALUE)
public class MerchantController {

    private final MerchantService merchantService;

    @PostMapping("/addNewMerchant")
    public ResponseEntity<String> addNewMerchant(@RequestHeader(name = "Authorization") String token, @RequestBody NewMerchantRequest param) throws JsonProcessingException {
        return merchantService.addNewMerchant(param, token);
    }

    @GetMapping("/getMerchantDetail")
    public ResponseEntity<String> getMerchantDetail(@RequestHeader(name = "Authorization") String token) throws JsonProcessingException {
        return merchantService.getDetailMerchantViewByCreatedBy(token);
    }

    @GetMapping("/getMerchantData")
    public ResponseEntity<String> getMerchantData(@RequestHeader(name = "Authorization") String token, @PathVariable String merchantUuid) throws JsonProcessingException {
        return merchantService.getDetailMerchantByUserView(merchantUuid);
    }

}
