package com.muf.springjpaimplement.constant;

public interface ConstantSQL {
    String getUserByUsernameOrEmailAndStatus = "SELECT * FROM public.usrs a WHERE (a.usrs_usrnme = :username or a.usrs_email = :username ) AND a.usrs_status = :status";
    String getUserByUsernameOrEmail = "SELECT * FROM public.usrs a WHERE a.usrs_usrnme = :username or a.usrs_email = :username";

    String getMerchantDetailByUserId = "SELECT " +
            "   m.mrchnt_uuid as merchantUuid, md.mrchnt_detl_nme as merchantDetailName, mc.mrchnt_ctegry_name as merchantCategoryName, " +
            "   mc.mrchnt_ctegry_uuid as merchantCategoryUuid, md.mrchnt_detl_phons as merchantDetailPhone, md.mrchnt_detl_city as merchantDetailCity, " +
            "   md.mrchnt_detl_prvnc as merchantDetailProvince, md.mrchnt_detl_pst_cde as merchantDetailPostCode, " +
            "   md.mrchnt_detl_addrss as merchantDetailPostCode, m.created_date as merchantCreatedDate " +
            "FROM public.mrchnt_detl md,  public.mrchnt m, public.mrchnt_ctegry mc " +
            "WHERE m.mrchnt_id = md.mrchnt_id AND m.mrchnt_ctegry_id = mc.mrchnt_ctegry_id AND md.created_by = :userId";

    String getGetMerchantDetailByMerchantUuid = "select " +
            "   m.mrchnt_uuid as merchantUuid, md.mrchnt_detl_nme as merchantDetailName, mc.mrchnt_ctegry_name as merchantCategoryName, " +
            "   mc.mrchnt_ctegry_uuid as merchantCategoryUuid, md.mrchnt_detl_phons as merchantDetailPhone, md.mrchnt_detl_city as merchantDetailCity, " +
            "   md.mrchnt_detl_prvnc as merchantDetailProvince, md.mrchnt_detl_pst_cde as merchantDetailPostCode, " +
            "   md.mrchnt_detl_addrss as merchantDetailPostCode, m.created_date as merchantCreatedDate " +
            "from public.mrchnt_detl md,  public.mrchnt m, public.mrchnt_ctegry mc " +
            "where m.mrchnt_id = md.mrchnt_id and m.mrchnt_ctegry_id = mc.mrchnt_ctegry_id and m.mrchnt_uuid = :merchantUuid";

    String getItemByCategoryUuid = "select " +
            "   c.ctegry_uuid as categoryUuid, m.mrchnt_uuid as merchantUuid, i.itms_uuid as itemUuid, i.itms_name as itemName, " +
            "   i.itms_stck as itemStock, i.itms_price as itemPrice, i.created_date as createdDate " +
            "from public.itms i, public.mrchnt m, public.ctegry c " +
            "where i.mrchnt_id = m.mrchnt_id and i.ctegry_id = c.ctegry_id and c.ctegry_uuid = :categoryUuid and i.itms_status = :itemStatus";

    String getCountItemByCategoryUuid = "select " +
            "   count(0) as counting " +
            "from public.itms i, public.mrchnt m, public.ctegry c " +
            "where i.mrchnt_id = m.mrchnt_id and i.ctegry_id = c.ctegry_id and c.ctegry_uuid = :categoryUuid and i.itms_status = :itemStatus";

    String getItemBySearch = "select " +
            "   c.ctegry_uuid as categoryUuid, m.mrchnt_uuid as merchantUuid, i.itms_uuid as itemUuid, i.itms_name as itemName, " +
            "   i.itms_stck as itemStock, i.itms_price as itemPrice, i.created_date as createdDate from public.itms i, public.mrchnt m, public.ctegry c " +
            "where i.mrchnt_id = m.mrchnt_id and i.ctegry_id = c.ctegry_id and i.itms_status = :itemStatus " +
            "and LOWER(concat_ws(' ', i.itms_uuid, i.itms_name, i.itms_stck, i.itms_price, i.created_date, c.ctegry_uuid, m.mrchnt_uuid)) like LOWER(CONCAT('%',:search,'%'))";

    String getCountItemBySearch = "select " +
            "   count(0) as counting " +
            "from public.itms i, public.mrchnt m, public.ctegry c " +
            "where i.mrchnt_id = m.mrchnt_id and i.ctegry_id = c.ctegry_id and i.itms_status = :itemStatus " +
            "and LOWER(concat_ws(' ', i.itms_uuid, i.itms_name, i.itms_stck, i.itms_price, i.created_date, c.ctegry_uuid, m.mrchnt_uuid)) like LOWER(CONCAT('%',:search,'%'))";

    String getItemListOnCart = "SELECT a.cart_itms_uuid as cartItemUuid, i.itms_name as itemName, i.itms_price as itemPrice, " +
            "   a.itms_qty as itemQty, c.cart_uuid as cartUuid, i.itms_uuid as itemUuid " +
            "FROM public.cart_itms a join public.itms i on a.itms_id = i.itms_id join public.cart c on c.cart_id = a.cart_id " +
            "WHERE a.created_by = :createdBy AND a.cart_itms_Status = :cartItemStatus ";

    String getItemCartItemUuidAndCartUuidAndIsdel = "SELECT " +
            "   a.* " +
            "FROM public.cart_itms a join public.itms i on a.itms_id = i.itms_id " +
            "join public.cart c on c.cart_id = a.cart_id WHERE i.itms_uuid = :itemUuid AND c.cart_uuid = :cartUuid AND a.cart_itms_status = :isdel ";

}
